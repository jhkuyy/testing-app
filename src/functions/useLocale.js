import {
  ref, provide, inject, computed,
} from '@vue/composition-api';
import appStorage, { AppStorage } from '../services/appStorage';
import i18n from '../i18n/Dictionaries';

const LocaleSymbol = Symbol('locale');


export const provideLocale = (language) => {
  const lang = ref(language);

  const dictionary = computed(() => i18n.getDictionaryByLocale(lang.value));

  provide(LocaleSymbol, {
    lang,
    dictionary: dictionary.value,
    translate: (key) => dictionary.value[key],
    setLang: (newLang) => {
      lang.value = newLang;
      appStorage.setItem(AppStorage.CURRENT_LANG, newLang);
    },
  });
};

export const useLocale = () => {
  const locale = inject(LocaleSymbol);

  if (!locale) {
    throw new Error('Locale is not provided');
  }

  return locale;
};
