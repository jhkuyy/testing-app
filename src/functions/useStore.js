import { provide, inject } from '@vue/composition-api';

const StoreSymbol = Symbol('store');

export const provideStore = (store) => provide(StoreSymbol, store);

export const useStore = () => {
  const store = inject(StoreSymbol);

  if (!store) {
    throw new Error('Store is not provided!');
  }

  return store;
};
