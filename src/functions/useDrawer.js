import { ref } from '@vue/composition-api';


function useDrawer() {
  const drawer = ref(false);

  function toggleDrawer() {
    drawer.value = !drawer.value;
  }

  return {
    drawer,
    toggleDrawer,
  };
}

export {
  useDrawer,
};
