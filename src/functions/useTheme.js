import { provide, inject } from '@vue/composition-api';

import appStorage, { AppStorage } from '../services/appStorage';

const ThemeSymbol = Symbol('theme');

export const provideTheme = (theme) => provide(ThemeSymbol, {
  theme,
  setTheme: (newTheme) => {
    appStorage.setItem(AppStorage.CURRENT_THEME, newTheme);
  },
});

export const useTheme = () => {
  const theme = inject(ThemeSymbol);

  if (!theme) {
    throw new Error('Store is not provided!');
  }

  return theme;
};
