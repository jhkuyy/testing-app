export const QuestionType = {
  SELECT: 'select',
  INPUT: 'input',
};

export const TestStep = {
  QUESTIONS: 'QUESTIONS',
  COMPLETE: 'COMPLETE',
  RESULTS: 'RESULTS',
};

export const Theme = {
  DARK: 'THEME_DARK',
  LIGHT: 'THEME_LIGHT',
};
