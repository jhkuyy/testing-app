import Ui from './Ui';
import Languages from './Languages';

export default { ...Ui, ...Languages };
