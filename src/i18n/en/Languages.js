import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Languages.RUSSIAN]: 'Russian',
  [DictionaryMapping.Languages.ENGLISH]: 'English',
};
