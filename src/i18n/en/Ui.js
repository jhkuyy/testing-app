import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Ui.APP_NAME]: 'App test',
  [DictionaryMapping.Ui.Question.Form.SUBMIT_BUTTON]: 'Next',
  [DictionaryMapping.Ui.Question.Form.COMPLETE_BUTTON]: 'Complete',
  [DictionaryMapping.Ui.Question.Form.INPUT_PLACEHOLDER]: 'Write the answer',

  [DictionaryMapping.Ui.NavigationDrawerMenu.HOME]: 'Home',
  [DictionaryMapping.Ui.NavigationDrawerMenu.SETTINGS]: 'Settings',

  [DictionaryMapping.Ui.Page.Settings.LANGUAGE]: 'Language',
  [DictionaryMapping.Ui.Page.Settings.THEME]: 'Theme',

  [DictionaryMapping.ColorTheme.DARK]: 'Dark',
  [DictionaryMapping.ColorTheme.LIGHT]: 'Light',

  [DictionaryMapping.Ui.Title.CONGRATULATIONS]: 'Congratulations',

  [DictionaryMapping.Ui.Component.QuestionTestComplete.TEST_COMPLETE]: 'Test complete',
  [DictionaryMapping.Ui.Component.QuestionTestComplete.SEE_RESULTS]: 'See results',
};
