import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Ui.APP_NAME]: 'Тестики',
  [DictionaryMapping.Ui.Question.Form.SUBMIT_BUTTON]: 'Далее',
  [DictionaryMapping.Ui.Question.Form.COMPLETE_BUTTON]: 'Завершить',
  [DictionaryMapping.Ui.Question.Form.INPUT_PLACEHOLDER]: 'Впишите ответ',

  [DictionaryMapping.Ui.NavigationDrawerMenu.HOME]: 'Главная',
  [DictionaryMapping.Ui.NavigationDrawerMenu.SETTINGS]: 'Настройки',

  [DictionaryMapping.Ui.Page.Settings.LANGUAGE]: 'Язык',
  [DictionaryMapping.Ui.Page.Settings.THEME]: 'Тема',

  [DictionaryMapping.ColorTheme.DARK]: 'Темная',
  [DictionaryMapping.ColorTheme.LIGHT]: 'Светлая',

  [DictionaryMapping.Ui.Title.CONGRATULATIONS]: 'Поздравляем',

  [DictionaryMapping.Ui.Component.QuestionTestComplete.TEST_COMPLETE]: 'Тест завершен',
  [DictionaryMapping.Ui.Component.QuestionTestComplete.SEE_RESULTS]: 'Посмотреть результаты',
};
