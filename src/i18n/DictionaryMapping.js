export default {
  Ui: {
    APP_NAME: 'UI_APP_NAME',
    Question: { // TODO: Move to component section
      Form: {
        INPUT_PLACEHOLDER: 'UI_QUESTION_FORM_INPUT_PLACEHOLDER',
        SUBMIT_BUTTON: 'UI_QUESTION_FORM_SUBMIT_BUTTON',
        COMPLETE_BUTTON: 'UI_QUESTION_FORM_COMPLETE_BUTTON',
      },
    },
    NavigationDrawerMenu: {
      HOME: 'UI_NAVIGATION_DRAWER_MENU_HOME',
      SETTINGS: 'UI_NAVIGATION_DRAWER_MENU_SETTINGS',
    },
    Page: {
      Settings: {
        LANGUAGE: 'UI_PAGE_SETTINGS_LANGUAGE',
        THEME: 'UI_PAGE_SETTINGS_THEME',
      },
    },
    Component: {
      QuestionTestComplete: {
        TEST_COMPLETE: 'UI_COMPONENT_QUESTION_TEST_COMPLETE_TEST_COMPLETE',
        SEE_RESULTS: 'UI_COMPONENT_QUESTION_TEST_COMPLETE_SEE_RESULTS',
      },
    },
    Title: {
      CONGRATULATIONS: 'Congratulations',
    },

  },
  Languages: {
    RUSSIAN: 'RUSSIAN',
    ENGLISH: 'ENGLISH',
  },
  ColorTheme: {
    LIGHT: 'LIGHT',
    DARK: 'DARK',
  },
};
