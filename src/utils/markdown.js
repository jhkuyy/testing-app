import marked from 'marked';
import DOMPurify from 'dompurify';

export const toHtml = (markdown) => marked(markdown) |> DOMPurify.sanitize;
