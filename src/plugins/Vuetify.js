import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import '@mdi/font/css/materialdesignicons.css';

import Dictionaries from '../i18n/Dictionaries';

Vue.use(Vuetify);

export default new Vuetify({
  locales: Dictionaries.dictionary,
  icons: {
    iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
  },
});
