import Vue from 'vue';
import VueRouter from 'vue-router';

import routeName from './routeName';
import Home from '../components/pages/Home.vue';
import Settings from '../components/pages/Settings.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: routeName.HOME,
    component: Home,
  },
  {
    path: '/settings',
    name: routeName.SETTINGS,
    component: Settings,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
