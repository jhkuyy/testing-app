import appStorage, { AppStorage } from '../services/appStorage';
import { Theme } from '../constants/enums';

const boot = () => ([
  appStorage.getItem(AppStorage.CURRENT_LANG) || 'en',
  appStorage.getItem(AppStorage.CURRENT_THEME) || Theme.LIGHT,
]);

export default boot;
