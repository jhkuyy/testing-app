module.exports = {
  lintOnSave: false,
  pwa: {
    name: 'Sereja`s testing app',
    themeColor: '#090f1e',
    msTileColor: '#090f1e',
    appleMobileWebAppStatusBarStyle: 'black-translucent',
    appleMobileWebAppCapable: 'yes',
    workboxPluginMode: 'GenerateSW',
    manifestOptions: {
      start_url: './',
      display: 'standalone',
      background_color: '#090f1e',
      description: 'Awesome tests by Seraja',
      orientation: 'portrait',
    },
  },
  transpileDependencies: [
    'vuetify',
  ],
};
