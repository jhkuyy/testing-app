module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/airbnb',
  ],
  rules: {
    camelcase: [
      'error',
      {
        properties: 'never',
        allow: [
          '__webpack_public_path__',
          'apg_49abeb65_467c_4060_9f5d_d725bd5df794',
        ],
      },
    ],
    'consistent-return': 'off',
    'implicit-arrow-linebreak': 'off',
    'import/extensions': [
      'error',
      'always',
      {
        jpeg: 'never',
        jpg: 'never',
        js: 'never',
        png: 'never',
      },
    ],
    'import/order': 'off',
    'import/prefer-default-export': 'off',
    indent: [
      'error',
      2,
      {
        SwitchCase: 1,
      },
    ],
    'linebreak-style': [
      'error',
      'unix',
    ],
    'max-len': 'off',
    'no-bitwise': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-param-reassign': 'off', // TODO fix
    'no-plusplus': 'off',
    // TODO: Remove next rule
    'no-throw-literal': 'off',
    'no-underscore-dangle': [
      'error',
      {
        allow: ['_lang', '__lang', '_scopeId'],
      },
    ],
    'no-unused-vars': [
      'error',
      {
        args: 'none',
        ignoreRestSiblings: true,
      },
    ],
    'operator-linebreak': [
      'error',
      'before',
      {
        overrides: {
          '=': 'ignore',
          '=>': 'ignore',
        },
      },
    ],
    'sort-imports': 'off',
    'vue/name-property-casing': 'off',
    'no-mixed-operators': 'off', // TODO fix
    'vue/require-default-prop': 'off', // TODO fix
    'func-names': 'off',
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  globals: {
    Chatra: true,
    Storage: false,
    __IS_DEV__: true,
    __webpack_public_path__: true,
    apg_49abeb65_467c_4060_9f5d_d725bd5df794: true,
    document: true,
    error: true,
    localStorage: false,
    window: true,
  },
};
